require(['jquery'], function($) {
    $(document).ready(function(){

        $(window).scroll(function() {
            if ($(this).scrollTop() < 100) {
                $('#scroll-top').hide();
            } else {
                $('#scroll-top').show();
            }
        });

        $('#scroll-top').click(function () {
            $(window).scrollTop(0);
        });

        $('#newsletterBox .yellow-box-close').click(function () {
            $('#newsletterBox').hide();
        });

    });
});