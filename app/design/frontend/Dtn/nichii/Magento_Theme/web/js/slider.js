require(['jquery','owlCarousel'], function($) {
    $(document).ready(function(){

        $('.owl-carousel.owl-theme').owlCarousel({
            items: 1,
            smartSpeed: 500,
            navSpeed: 500,
            // autoplay: true,
            // autoplayTimeout: 6000,
            autoplayHoverPause: true,
            dots: true
        });

        $('.owl-carousel.product-items').owlCarousel({
            items: 6,
            smartSpeed: 500,
            navSpeed: 500,
            nav: true,
            dots: false
        });

        $('.owl-carousel.instagram').owlCarousel({
            items: 5,
            smartSpeed: 500,
            navSpeed: 500,
            nav: true,
            dots: false
        });
    });
});