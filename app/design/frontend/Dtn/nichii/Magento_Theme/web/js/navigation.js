require(['jquery'], function($) {
    $(document).ready(function(){

        $("ul.level0.submenu").prepend("<li class='level1 nav-3-0'><img src=\"https://www.nichii.com/pub/media/wysiwyg/nichii/nichii_Mega_Menu_Banner_195x320_20180426_DRAFT1.jpg\" width=\"195\" height=\"320\"></li>");

        $(window).scroll(function() {
            if (!$('.nav-sections .navigation #ui-id-2 > .minicart-wrapper').length) {
                $('.header.content .right-column .minicart-wrapper').clone().appendTo('.nav-sections .navigation #ui-id-2');
            }

            if ($(this).scrollTop() > 100) {
                $('.header.content .right-column .minicart-wrapper').hide();
                $('.nav-sections .navigation #ui-id-2 > .minicart-wrapper').show();
            } else {
                $('.header.content .right-column .minicart-wrapper').show();
                $('.nav-sections .navigation #ui-id-2 > .minicart-wrapper').hide();
            }
        });

    });
});