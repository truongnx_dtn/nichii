var config = {
    // map: {
    //     '*': {
    //         'owlCarousel': 'js/jquery/owl.carousel'
    //     }
    // },
    paths: {
        'owlCarousel': 'js/carousel/owl.carousel.min'
    },

    shim: {
    	'owlCarousel': {
    		'deps': ['jquery']
    	}
    }
};
